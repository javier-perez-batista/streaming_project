<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 25/03/2018
 * Time: 19:32
 */

return [
    'api_key' => 'API_KEY',
    'api_secret' => 'API_SECRET',
    'api_app_id' => 'API_APP_ID',
    'location' => 'us-east',
    'timeout' => 3,
];