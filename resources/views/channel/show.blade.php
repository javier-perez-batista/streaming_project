@extends('layouts.master')
@section('videoStream')
    <div class="row" style="margin: 2% 0%;">


        <div class="col-lg-10" >
            <div id="before_stream">
                <iframe src="https://player.twitch.tv/?channel={{$channel->url}}" frameborder="0" allowfullscreen="true" scrolling="no" height="720" width="1280"></iframe>
            </div>

            <div id="channel_lateral">
                @include('channel.lateral')
            </div>
        </div>
        <div class="col-lg-2" style="padding: 0" id="channel_lateral_chat">
            <iframe src="https://www.twitch.tv/embed/{{$channel->url}}/chat" frameborder="0" scrolling="no" height="780" width="100%"></iframe>
        </div>

    </div>
    <script src="https://embed.twitch.tv/embed/v1.js"></script>

    <!-- Create a Twitch.Embed object that will render within the "twitch-embed" root element. -->
    <script type="text/javascript">
        var options = {
            width: 854,
            height: 480,
            channel: "{{$channel->url}}",
        };
        var player = new Twitch.Player("embed_live_stream", options);
        player.setVolume(0.5);
    </script>
@endsection
