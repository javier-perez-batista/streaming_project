<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 16/05/2018
 * Time: 18:50
 */?>
<div>
    <div style="padding-bottom: .3rem;"><a class="streaming_footer" href="/streaming_project/public/channel/{{$channel->id}}">{{$channel->title}}{{$channel->name}}</a>
        @if($channels->contains('id',$channel->id))
            <button style="float: right" class="dejar_de_seguir" onclick="unfollowChannel({{$channel->id}})">Dejar de Seguir</button>
        @else
            <button style="float: right" class="btn_jugar" onclick="followChannel({{$channel->id}})">Seguir</button>
        @endif
    </div>
    <div><a class="streaming_footer" href="/streaming_project/public/user/{{$user->id}}">{{$user->username}}</a><span> jugando </span><a class="streaming_footer"
                href="/streaming_project/public/juegos/{{$juego->id}}">{{$juego->name}}</a>
        <span style="float: right;">{{$channel->subscribers}} suscriptores</span>
    </div>
</div>

