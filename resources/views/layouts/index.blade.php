@extends('layouts.master')

@section('videoStream')

    <div class="row" style="margin: 2% 0%;">
        <div class="col-lg-4">
            <div class="col-lg-4" style="float: left">
                <div><img class="img-thumbnail-custom" src="{{asset('/storage/images')}}/{{$channel->user->profile_picture}}" alt=""></div>

                <div>
                    <div class="boton_perfil_index" id="boton_profile">
                        @include('user.botonPerfil')
                    </div>
                </div>
            </div>
            <div class="col-lg-8" style="float: right">
                <div class="top_profile_name"><a class="streaming_footer" href="/streaming_project/public/user/{{$channel->user->id}}"><h4>{{$channel->user->username}}</h4></a></div>
                <div class="contenidos_profile" style="margin-top: 0;">

                    <div class="myInfo_contenido">
                        <div>Nombre : {{$channel->user->name}}</div>
                        <div>Edad : {{$channel->user->age}}</div>
                        <div>Correo : {{$channel->user->email}}</div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-5">
            <iframe src="https://player.twitch.tv/?channel={{$channel->url}}" frameborder="0" allowfullscreen="true" scrolling="no" height="378" width="620"></iframe>
            <div id="channel_lateral">
                @include('channel.lateral')
            </div>
        </div>
        <div class="col-lg-3" id="channel_lateral">
            @include('chat')
        </div>

    </div>


    <script src="https://embed.twitch.tv/embed/v1.js"></script>

    <!-- Create a Twitch.Embed object that will render within the "twitch-embed" root element. -->
    <script type="text/javascript">
        var options = {
            width: 854,
            height: 480,
            channel: "iceblueman",
        };
        var player = new Twitch.Player("embed_live_stream", options);
        player.setVolume(0.5);
    </script>

@endsection



@section('content2')
    <div class="box">
        <div class=" toogle_switch" style="width: 100%;position: relative;height: 100%;">
            <div onclick="verCanales()" id="canales_separador" class=" toogle" >CANALES</div>
            <div onclick="verJuegos()" id="juegos_separador" class=" toogle toogle_selected" >JUEGOS</div>
        </div>
    </div>
    <div id="channels-juegos" class="row col-lg-12">
        @include('channel.channels')
    </div>
@endsection
