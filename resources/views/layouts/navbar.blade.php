<a href="/streaming_project/public/" class="navbar_name_logo navbar_section">STREAM</a>

    <a class="navbar_section" href="/streaming_project/public/explorar">Explorar</a>
    <a class="navbar_section" href="/streaming_project/public/torneos">Torneos</a>
    <a class="navbar_section" href="/streaming_project/public/equipos">Equipos</a>

<div class="navbar_input_search">
    <form id="search_form" method="POST" action="/streaming_project/public/">
        {{csrf_field()}}
        <div class="form-group">
            <input type="text" name="search_navbar" id="search_navbar" placeholder="search...">
            <button type="submit" class="search_navbar_button" hidden ></button>
        </div>
    </form>
</div>

<div id="nabvar_user" onclick="showProfileOptions()" class="navbar_user navbar_section">
    <img src="{{asset('/storage/images/')}}/{{$logged_user->profile_picture}}" alt="">
    <span>{{$logged_user->username}}</span>
    <i id="caret" class="fa fa-caret-down"></i>
</div>
<div id="profile_options" class="profile_options hidden" onmouseleave="hideProfileOptions()">
    <div>
        <a style="width: 100%;" class="navbar_section" href="/streaming_project/public/user/{{$logged_user->id}}"><span>Perfil</span></a>
    </div>
    <div class="logOut" onclick="logOut()">
        <span class="navbar_section">Log-out</span>
    </div>
</div>
