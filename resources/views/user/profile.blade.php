@extends('layouts.master')

@section('content')
    <div class="row profile_banner">
        <img src="{{asset('/storage/images/')}}/{{$user->banner_image}}" alt="">
        <div class="image_profile">
            <img src="{{asset('/storage/images/')}}/{{$user->profile_picture}}" alt="">
        </div>
        <div class="boton_perfil" id="boton_profile">
            @include('user.botonPerfil')
        </div>


    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="contenidos_profile">
                <div class="profiles_headers">MY INFO</div>
                <div class="myInfo_contenido">
                    <div>Nombre : {{$user->name}}</div>
                    <div>Edad : {{$user->age}}</div>
                    <div>Correo : {{$user->email}}</div>
                </div>

            </div>
        </div>
        <div class="col-lg-3">
            <div class="contenidos_profile">
                <div class="profiles_headers">ABOUT ME</div>
                <div id="aboutMe_contenido" onfocusout="cerrarEditarAboutMe()" class="aboutMe_contenido" @if($user->id == $logged_user->id)onclick="editarAboutMe()"@endif>
                    <div>{{$user->aboutme}}</div>
                </div>
            </div>

        </div>
        <div class="col-lg-3">
            <div class="contenidos_profile">
                <div class="profiles_headers">MY RRSS</div>
                @if($user->twitter!='')<div class="rrss"><img src="{{asset('/storage/rrss_images/twitter.svg')}}" alt=""><div class="rrss_name">
                        <a class="streaming_footer" href="https://twitter.com/{{$user->twitter}}">{{$user->twitter}}</a></div></div>@endif
                @if($user->facebook!='')<div class="rrss"><img src="{{asset('/storage/rrss_images/facebook.svg')}}" alt=""><div class="rrss_name">
                        <a class="streaming_footer" href="https://www.facebook.com/{{$user->facebook}}">{{$user->facebook}}</a></div></div>@endif
                @if($user->youtube!='')<div class="rrss"><img src="{{asset('/storage/rrss_images/youtube.svg')}}" alt=""><div class="rrss_name">
                        <a class="streaming_footer" href="https://www.youtube.com/{{$user->youtube}}">{{$user->youtube}}</a></div></div>@endif
                @if($user->instagram!='')<div class="rrss"><img src="{{asset('/storage/rrss_images/instagram.svg')}}" alt=""><div class="rrss_name">
                        <a class="streaming_footer" href="https://www.instagram.com/{{$user->instagram}}">{{$user->instagram}}</a></div></div>@endif
            </div>
        </div>
        <div class="col-lg-3">
            <div class="contenidos_profile">
                <div class="profiles_headers">MY GAMES</div>
                <ul class="ul_favs_games">
                    @if($user->id == $logged_user->id)
                        @foreach($favs_l as $game)
                            <li><a href="/streaming_project/public/juegos/{{$game->id}}"><div class="rrss"><img src="{{asset('storage/images/'.$game->thumbnail)}}"
                                                                                                                alt="">{{$game->name}}</div></a></li>
                        @endforeach
                    @else
                        @foreach($favs_u as $game)
                            <li><a href="/streaming_project/public/juegos/{{$game->id}}"><div class="rrss"><img src="{{asset('storage/images/'.$game->thumbnail)}}"
                                                                                                                alt="">{{$game->name}}</div></a></li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection
