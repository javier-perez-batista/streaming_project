<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 17/05/2018
 * Time: 19:11
 */
?>
@if(!isset($user))
    @if(!$friends->contains('id',$channel->user->id) && $channel->user->id != $logged_user->id)
        <button id="add_friend" class="btn_jugar" onclick="addFriend({{$channel->user->id}})" >Añadir Amigo</button>
    @elseif($channel->user->id == $logged_user->id)
        <span>Imagen de perfil</span><input type="file">
        <span>Banner de perfil</span><input type="file">
    @else
        <button id="unfriend" class="dejar_de_seguir" onclick="unFriend({{$channel->user->id}})" >Quitar Amigo</button>
    @endif
@else
    @if(!$friends->contains('id',$user->id) && $user->id != $logged_user->id)
        <button id="add_friend" class="btn_jugar" onclick="addFriend({{$user->id}})" >Añadir Amigo</button>
    @elseif($user->id == $logged_user->id)
        <span>Imagen de perfil</span><input type="file">
        <span>Banner de perfil</span><input type="file">
    @else
        <button id="unfriend" class="dejar_de_seguir" onclick="unFriend({{$user->id}})" >Quitar Amigo</button>
    @endif
@endif