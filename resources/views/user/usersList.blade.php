<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 19/05/2018
 * Time: 15:14
 */
?>
@foreach($users as $user)
    <div class="juego_thumnail item torneos_contenido_selected">
        <img class="img-thumbnail-custom" src="{{asset('/storage/images')}}/{{$user->profile_picture}}" alt="">
        <a class="streaming_footer" href="/streaming_project/public/user/{{$user->id}}">{{$user->username}}</a>
    </div>
@endforeach
