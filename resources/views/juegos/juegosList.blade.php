@foreach($juegos as $juego)
    <div class="juego_thumnail torneos_contenido_selected">
        <img class="img-thumbnail-custom" src="{{asset('/storage/images')}}/{{$juego->thumbnail}}" alt="">
        <a class="streaming_footer" href="/streaming_project/public/juegos/{{$juego->id}}">{{$juego->name}}</a>
    </div>
@endforeach

