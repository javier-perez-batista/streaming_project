@extends('layouts.master')

@section('content')
    <div class="row profile_banner">
        <img src="{{asset('/storage/images/')}}/{{$juego->banner_image}}" alt="">
        <div class="image_profile">
            <img src="{{asset('/storage/images/')}}/{{$juego->thumbnail}}" alt="">
        </div>
        <div class="boton_perfil" id="boton_juego">
            @include('juegos.botonFav')
        </div>
    </div>
    <div class="row " style="height: 6%;">
        <div class="tabs_juegos">
            <div class="tab_canales active_pestanyas" id="tab_canales" onclick="cambiarPestanya('{{$juego->id}}',0)">
                <span>Canales</span>
            </div>
            <div class="tab_canales" id="tab_videos" onclick="cambiarPestanya('{{$juego->id}}',1)">
                <span>Videos</span>
            </div>
        </div>
        <div class="col-lg-12" id="contenido_pestanyas_juego">
            @include('channel.channels')
        </div>
    </div>

@endsection
