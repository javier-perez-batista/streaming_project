<?php
/**
 * Created by PhpStorm.
 * User: javi_
 * Date: 21/05/2018
 * Time: 19:30
 */?>
@if(!preg_match("/$juego->id/","'".$logged_user->favs_games."'"))
    <button onclick="addFavoritos({{$juego->id}})" class="btn_jugar">+ Favoritos</button>
@else
    <button onclick="quitarFavoritos({{$juego->id}})" class="dejar_de_seguir">- Favoritos</button>
@endif
