@extends('layouts.master')

@section('content')
    <div class="list_title">
        <div class="list_title_interno">
            <div class="float-left"><h3>Canales</h3></div><div class="float-left number_items"><span>{{sizeof($channelsAll)}}</span></div>
        </div>
    </div>
    <div class="list_channels">
        @include('channel.channels')
    </div>
    <div class="list_title">
        <div class="list_title_interno">
            <div class="float-left"><h3>Juegos</h3></div><div class="float-left number_items"><span>{{sizeof($juegos)}}</span></div>
        </div>
    </div>
    <div class="list_juegos">
        @include('juegos.juegosList')
    </div>
    <div class="list_title">
        <div class="list_title_interno">
            <div class="float-left"><h3>Usuarios</h3></div><div class="float-left number_items"><span>{{sizeof($users)}}</span></div>
        </div>
    </div>
    <div class="list_users">
        @include('user.usersList')
    </div>
@endsection