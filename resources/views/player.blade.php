@extends('layouts.master')
@section('content')
    <div class="row">
        <div class="col-lg-9" style="margin-top: 1rem;">
            <video id="example_video_1" class="video-js vjs-default-skin vjs-big-play-centered"
                   controls preload="auto" height="600" width="980">
                <source src="{{asset("/storage/images/videos/$video->name.mp4")}}" type="{{$mime}}" />
            </video>
            <div class="video_footer">
                <span class="">{{$video->name}}</span>
            </div><div class="video_footer">
                <a class="streaming_footer" href="/streaming_project/public/user/{{$video->channel->user->id}}">{{$video->channel->user->username}}  </a><span> jugando </span><a class="streaming_footer" href="/streaming_project/public/juegos/{{$video->juego->id}}">{{$video->juego->name}}</a><span> en </span><a class="streaming_footer" href="/streaming_project/public/channel/{{$video->channel->id}}">  {{$video->channel->name}}</a>
            </div>
        </div>
        <div class="video_sugerencias col-lg-3">
            <div class="span_canales sugerencias_titulo">
                <span>Sugerencias</span>
            </div>
            @include('videoList')
        </div>
    </div>
@endsection