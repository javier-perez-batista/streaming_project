@extends('layouts.master')

@section('content')
    <div class="row explorar_selector">
        <select class="explorar_select" onchange="getExplorarElements()" name="elementos_explorar" id="elementos_explorar">
            <option selected value="0">Juegos</option>
            <option value="1">Canales</option>
            <option value="2">Usuarios</option>
        </select>

    </div>
    <div id="explora_list">
        @include('juegos.juegosList')
    </div>
@endsection