var urlLocation = '/streaming_project/public/user/';
function addFriend(id) {
    var request = $.get(urlLocation+'addfriend/'+id);
    var container = $('#boton_profile');
    request.done(function (data) {
        container.html(data);
        reloadFriends();
    });
}
function unFriend(id) {
    var request = $.get(urlLocation+'unfriend/'+id);
    var container = $('#boton_profile')
    request.done(function (data) {
        container.html(data);
        reloadFriends();
    });
}

function reloadFriends() {
    var request = $.get(urlLocation+'reload/friends/');
    var container = $('#amigos_que_sigo');
    request.done(function (data) {
        container.html(data);
    });
}

function editarAboutMe() {
    var container = $('#aboutMe_contenido');
    var hijo = container.children();
    if(hijo.is('div')){
        var contenido = hijo.html();
        container.html('<textarea rows="4" cols="50">'+contenido.trim()+'</textarea>');
        container.children().focus();
    }
}

function cerrarEditarAboutMe() {
    var container = $('#aboutMe_contenido');
    var hijo = container.children();
    var contenido = hijo.val().trim();
    var request = $.get(urlLocation+'actualiza/aboutme/'+contenido);
    request.done(function () {
        container.html('<div>'+contenido+'</div>');
    });
}