var urlChannel = '/streaming_project/public/channel/';
function followChannel(id){
    var request = $.get(urlChannel+'follow/'+id);
    request.done(function (data) {
        $('#channel_lateral').html(data);
        reloadChannels();
    });

}
function unfollowChannel(id){
    var request = $.get(urlChannel+'unfollow/'+id);
    request.done(function (data) {
        $('#channel_lateral').html(data);
        reloadChannels();
    });

}

function reloadChannels() {
    var request2 = $.get(urlChannel+'reload/channels/');
    var container = $('#canales_que_sigo');
    request2.done(function (data) {
        container.html(data);

    });
}

$(document).ready(function () {
    setInterval(showChat,3000);
});

function showChat() {
    var request = $.get('/streaming_project/public/traeChat')
    request.done(function (data) {
        $('#chat_cuerpo').append(data);
    });
}

function enviar() {
    var texto = $('#text_area_enviar').val();
    var request = $.get('/streaming_project/public/traeChat/'+texto)
    request.done(function (data) {
        $('#chat_cuerpo').append(data);
        $('#text_area_enviar').val('');
    });
}