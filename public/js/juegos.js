function addFavoritos(id){
    var request = $.get('addFavoritos/'+id);
    var container = $('#boton_juego');
    request.done(function (data) {
        container.html(data);
    });
}

function quitarFavoritos(id){
    var request = $.get('quitarFavoritos/'+id);
    var container = $('#boton_juego');
    request.done(function (data) {
        container.html(data);
    });
}

function cambiarPestanya(juego,id){
    var request = $.get('cambiarPestanya/'+juego+'/'+id);
    var container = $('#contenido_pestanyas_juego');
    request.done(function (data) {
        switch (id){
            case 0:
                $('#tab_canales').addClass('active_pestanyas');
                $('#tab_videos').removeClass('active_pestanyas');
                break;
            case 1:
                $('#tab_canales').removeClass('active_pestanyas');
                $('#tab_videos').addClass('active_pestanyas');
                break;
        }
        container.html(data);
    });
}