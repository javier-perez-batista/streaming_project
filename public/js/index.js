function verCanales() {
    if($('#juegos_separador').hasClass('toogle_selected'))return false;
    var request = $.get('index/ver/channels/');
    $('#canales_separador').removeClass('toogle_selected');
    $('#juegos_separador').addClass('toogle_selected');
    var container = $('#channels-juegos');
    request.done(function (data) {
        container.html(data);
        $('#canales_separador').removeClass('toogle_selected');
        $('#juegos_separador').addClass('toogle_selected');
    });
}

function verJuegos() {
    if($('#canales_separador').hasClass('toogle_selected'))return false;
    var request = $.get('index/ver/juegos/');
    $('#juegos_separador').removeClass('toogle_selected');
    $('#canales_separador').addClass('toogle_selected');
    var container = $('#channels-juegos');
    request.done(function (data) {
        container.html(data);
        $('#juegos_separador').removeClass('toogle_selected');
        $('#canales_separador').addClass('toogle_selected');
    });
}

function showProfileOptions() {
    $('#profile_options').removeClass('hidden');
    $('#caret').removeClass('fa-caret-down');
    $('#caret').addClass('fa-caret-up');
    $('#nabvar_user').focus()
    event.stopPropagation();
}

function hideProfileOptions() {
    $('#profile_options').addClass('hidden');
}

function logOut(){
    var request = $.get('/streaming_project/public/logout/');
    request.done(function () {
        window.location.href = "/streaming_project/public/login";
    });
}