<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    public function users(){
        return $this->hasMany(User::class);
    }
    public function juego(){
        return $this->belongsTo(Juego::class);
    }
}
