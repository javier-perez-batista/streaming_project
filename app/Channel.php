<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Channel extends Model
{
    //
    protected $fillable = ['search_navbar'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
