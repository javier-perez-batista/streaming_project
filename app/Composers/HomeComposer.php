<?php
namespace App\Composers;

use Illuminate\Support\Facades\Auth;

class HomeComposer
{

    public function compose($view)
    {
        //Add your variables
        $view->with('logged_user',Auth::user());
    }
}