<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Juego extends Model
{
    public function tournaments(){
        return $this->hasMany(Tournament::class);
    }
}
