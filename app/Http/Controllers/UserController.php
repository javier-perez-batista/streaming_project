<?php

namespace App\Http\Controllers;


use App\Channel;
use App\Juego;
use App\RelationUserUser;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /*
     * Mostrar perfil de un usuario
     * @param int id
     * @return Response
     */
    public function showProfile($id){
        $user = User::findOrFail($id);
        $favs_games_user = $user->favs_games;
        $favs_games_user = explode(',',$favs_games_user );
        $favs = [];
        foreach ($favs_games_user as $fav_g) {
            array_push($favs,$fav_g);
        }
        $favs_u = collect($favs);
        $favs_u = Juego::all()->whereIn('id',$favs_u);
        $logged_user = Auth::user();
        $favs_games_user = $logged_user->favs_games;
        $favs_games_user = explode(',',$favs_games_user );
        $favs = [];
        foreach ($favs_games_user as $fav_g) {
            array_push($favs,$fav_g);
        }
        $favs_l = collect($favs);
        $favs_l = Juego::all()->whereIn('id',$favs_l);
        return view('user.profile')->with(compact('user','favs_u','favs_l'));
    }

    public function followProfile($id){
        $logged_user = Auth::user();
        $user = User::findOrFail($id);
        $relation = new RelationUserUser();
        $relation->user_followed = $id;
        $relation->user_follower = $logged_user->id;
        $relation->save();
        return view('user.profile')->with(compact('user','logged_user'));
    }

    public function addFriend($id_user){
        $relation = new RelationUserUser();
        $relation->user_followed = $id_user;
        $relation->user_follower = Auth::id();
        $relation->save();

        $user = User::findOrFail($id_user);
        $logged_user = Auth::id();
        $friends = DB::select("SELECT * FROM users WHERE id IN (SELECT user_followed FROM relation_users_users WHERE user_follower='$logged_user')");
        $friends = collect($friends);
        $html = view('user.botonPerfil',compact('user','friends'))->render();
        return $html;
    }

    public function unFriend($id_user){
        RelationUserUser::where('user_followed','=',"$id_user")->where('user_follower','=',Auth::id())->delete();
        $user = User::findOrFail($id_user);
        $logged_user = Auth::id();
        $friends = DB::select("SELECT * FROM users WHERE id IN (SELECT user_followed FROM relation_users_users WHERE user_follower='$logged_user')");
        $friends = collect($friends);
        $html = view('user.botonPerfil')->with(compact('user','friends'))->render();

        return $html;
    }

    public function reloadFriends(){
        $logged_user = Auth::id();
        $friends = DB::select("SELECT * FROM users WHERE id IN (SELECT user_followed FROM relation_users_users WHERE user_follower='$logged_user')");
        $friends = collect($friends);
        $html = view('layouts.sideBarFriends')->with(compact('friends'))->render();
        return $html;
    }

    public function actualizaAboutMe($contenido){
        $user = User::findOrFail(Auth::id());
        $user->aboutme = $contenido;
        $user->save();
    }
}
