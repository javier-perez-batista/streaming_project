<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Juego;
use App\RelationTournamentUser;
use App\RelationUserChannel;
use App\Tournament;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function cambiaPestanya($id_platform) {
        $torneos = Tournament::all();
        if($id_platform>0)$torneos = Tournament::all()->where('platform','=',"$id_platform");
        $torneos_apuntados = RelationTournamentUser::all()->where('user_id','=',Auth::id());
        $html = view('torneos.torneos', compact('torneos','torneos_apuntados'))->render();
        return $html;
    }
    public function jugarTorneo($id_torneo){
        $relation = new RelationTournamentUser();
        $relation->user_id = Auth::id();
        $relation->tournament_id = $id_torneo;
        $relation->save();
        $torneo = Tournament::findOrFail($id_torneo);
        $torneo->actual_players = $torneo->actual_players+1;
        $torneo->save();
        $torneos = Tournament::all()->where('id','=',"$id_torneo");
        $torneos_apuntados = RelationTournamentUser::all()->where('user_id','=',Auth::id());
        $html = view('torneos.torneos')->with(compact('torneos','torneos_apuntados'))->render();
        return $html;
    }

    public function dejarTorneo($id_torneo){
        $torneo = Tournament::findOrFail($id_torneo);
        $torneo->actual_players = $torneo->actual_players-1;
        $torneo->save();
        RelationTournamentUser::where('tournament_id','=',"$id_torneo")->where('user_id','=',Auth::id())->delete();

        $torneos = Tournament::all()->where('id','=',"$id_torneo");
        $torneos_apuntados = RelationTournamentUser::all()->where('user_id','=',Auth::id());
        $html = view('torneos.torneos')->with(compact('torneos','torneos_apuntados'))->render();
        return $html;
    }

    public function filtrarJuegos($input){
        //$juegos = Juego::all()->where('name','LIKE','%'.$input.'%');
        $juegos = DB::select("SELECT * FROM juegos WHERE name LIKE '%$input%'");
        $juegos = collect($juegos);
        $html = view('juegos.juegosDiv')->with(compact('juegos'));
        return $html;
    }

    public function juegosFiltrados($id_juego,$id_plataforma){
        if($id_plataforma == 0){
            $torneos = DB::select("SELECT * FROM tournaments WHERE juego_id=$id_juego");
            $torneos = Tournament::all()->where('juego_id','=',"$id_juego");
        }else{
            $torneos = DB::select("SELECT * FROM tournaments WHERE platform=$id_plataforma AND juego_id=$id_juego");
            $torneos = Tournament::all()->where('juego_id','=',"$id_juego")->where('platform','=',"$id_plataforma");
        }
        $torneos = collect($torneos);
        $torneos_apuntados = RelationTournamentUser::all()->where('user_id','=',Auth::id());
        $html = view('torneos.torneos', compact('torneos','torneos_apuntados','juego'))->render();
        return $html;
    }

    public function verJuegos(){
        $juegos = Juego::all();
        $html = view('juegos.juegosList')->with(compact('juegos'))->render();
        return $html;
    }

    public function verChannels(){
        $channelsAll = Channel::all();
        $html = view('channel.channels')->with(compact('channelsAll'))->render();
        return $html;
    }
    public function verUsers(){
        $users = User::all();
        $html = view('user.usersList')->with(compact('users'))->render();
        return $html;
    }
    public function logOut(){
        Auth::logout();
    }

}

