<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Juego;
use App\RelationUserChannel;
use App\RelationUserUser;
use App\User;
use App\Video;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $user_id = Auth::id();
            view()->share('logged_user', Auth::user());
            $channels = DB::select("SELECT * FROM channels WHERE id IN (SELECT channel_id FROM relation_channels_users WHERE user_id=$user_id)");
            view()->share('channels',collect($channels));
            view()->share('channelsAll',Channel::all());
            view()->share('juegos',Juego::all());
            $friends = DB::select("SELECT * FROM users WHERE id IN (SELECT user_followed FROM relation_users_users WHERE user_follower=$user_id)");
            $friends = collect($friends);
            view()->share('videos',Video::all());
            view()->share('friends',collect($friends));
            return $next($request);
        });
    }
}
