<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Juego;
use App\User;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JuegoController extends Controller
{
    public function index(){
        return view('juegos.juegos');
    }

    public function show($id){
        $juego = Juego::findOrFail($id);
        $channelsAll = Channel::all()->where('game','=',"$id");
        return view('juegos.show')->with(compact('juego','channelsAll'));
    }

    public function addFavoritos($id){
        $user = User::findOrFail(Auth::id());
        if($user->favs_games != '')$user->favs_games = $user->favs_games.','.$id;
        else $user->favs_games = $id;
        $user->save();
        $juego = Juego::findOrFail($id);
        $channelsAll = Channel::all()->where('game','=',"$id");
        Auth::user()->favs_games = Auth::user()->favs_games.','.$id;
        $logged_user = Auth::user();
        $html = view('juegos.botonFav')->with( compact('juego','channelsAll','logged_user'))->render();
        return $html;
    }

    public function quitarFavoritos($id){
        $user = User::findOrFail(Auth::id());
        $user->favs_games = str_replace("$id","",$user->favs_games);
        $user->favs_games = str_replace(",,",",",$user->favs_games);
        if(substr("$user->favs_games",0,1) == ',')$user->favs_games = str_replace_first(",","",$user->favs_games);
        if(substr("$user->favs_games",-1) == ',')$user->favs_games = str_replace_first(",","",$user->favs_games);
        $user->save();
        $juego = Juego::findOrFail($id);
        $channelsAll = Channel::all()->where('game','=',"$id");
        Auth::user()->favs_games = $user->favs_games;
        $logged_user = Auth::user();
        $html = view('juegos.botonFav')->with( compact('juego','channelsAll','logged_user'))->render();
        return $html;
    }

    public function cambiarPestanya($juego,$id){
        switch ($id){
            case 0:
                $channelsAll = Channel::all()->where('game','=',"$juego");
                $html = view('channel.channels')->with(compact('channelsAll'))->render();
                break;
            case 1:
                $videos = Video::all()->where('juego_id','=',"$juego");
                $html = view('videoList')->with(compact('videos'))->render();
                break;
        }
        return $html;
    }
}
