<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Video;
use App\VideoStream;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class videoStreamController extends Controller{

    public function loadVideo($filename){
        $videosDir = base_path('resources/assets/videos');
        if (file_exists($filePath = $videosDir."/".$filename)) {
            $stream = new VideoStream($filePath);
            return response()->stream(function() use ($stream) {
                $stream->start();
            });
        }
        return response("File doesn't exists", 404);
    }


    public function playVideo($id){
        $video = Video::findOrFail($id);
        //$video = "video_samurai.mp4";
        $mime = "video/mp4";
        $title = "Os Simpsons";
        return view('player')->with(compact('video', 'mime', 'title'));
    }


}
