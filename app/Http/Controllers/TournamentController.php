<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Juego;
use App\Platform;
use App\RelationTournamentUser;
use App\Tournament;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TournamentController extends Controller
{
    public function index($platform = null){
        $juegos = Juego::all();
        if($platform!=null){
            $platform_id = Platform::all()->where('name','like',"$platform")->first();
            $platform_id = $platform_id->id;
            $torneos = Tournament::all()->where('platform','=',"$platform_id");
        }else{
            $torneos = Tournament::all();
        }
        $torneos_apuntados = RelationTournamentUser::all()->where('user_id','=',Auth::id());
        return view('torneos.index')->with(compact('juegos','torneos','platform','platform_id','torneos_apuntados'));
    }

    public function buscar(Request $request){
        $id_juego = Juego::findOrFail(request('id_juego'));
        $torneos = Tournament::all()->where('id_juego',$id_juego);
        return view('torneos.torneos')->with(compact('torneos'));
    }

    public function search(){
        $juegos = Juego::all();
        $channels = Channel::all()->take(10);
        return view('torneos.index')->with(compact('juegos','channels'));
    }

    public function unirse($platform = null){
        $torneo_a_unirse = request('torneo_id_unirse');
        $relation = new RelationTournamentUser();
        $relation->user_id = Auth::id();
        $relation->tournament_id = $torneo_a_unirse;
        $relation->save();
        $this->index();
        $juegos = Juego::all();
        $channels = Channel::all()->take(10);
        if($platform){
            $platform_id = Platform::all()->where('name','like',"%$platform%");
            $torneos = Tournament::all()->where('platform','=',"$platform_id");
        }else{
            $torneos = Tournament::all();
        }
        return view('torneos.index')->with(compact('juegos','channels','torneos'));
    }
}
