<?php

namespace App\Http\Controllers;

use App\chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function nuevoTexto(){
        $random = rand(1,200);
        $chat = Chat::findOrFail($random);
        $html = view('chatMessage')->with(compact('chat'))->render();
        return $html;
    }
    public function enviarTexto($texto){
        $relation = new chat;
        $relation->message = $texto;
        $relation->user_id = Auth::id();
        $relation->save();
        $chat = chat::findOrFail($relation->id);
        $html = view('chatMessage')->with(compact('chat'))->render();
        return $html;
    }

}
