<?php

namespace App\Jobs;

use App\Channel;
use App\RelationUserChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;

class followChannel implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id=$id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $logged_user = Auth::user();
        $channel = Channel::findOrFail($this->id);
        $relation = new RelationUserChannel();
        $relation->user_id = $logged_user->id;
        $relation->channel_id = $channel->id;
        $relation->save();
    }
}
