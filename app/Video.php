<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //

    public function juego(){
        return $this->belongsTo(Juego::class);
    }
    public function channel(){
        return $this->belongsTo(Channel::class);
    }
}
