<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username',30);
            $table->string('name',30)->default("");
            $table->string('email',100)->unique();
            $table->string('password');
            $table->integer('state')->default(1);
            $table->string('profile_picture',100)->default("");
            $table->string('banner_image',100)->default("");
            $table->integer('age')->default(18);
            $table->string('favs_games',100)->default("");
            $table->string('twitter',40)->default("");
            $table->string('facebook',40)->default("");
            $table->string('youtube',40)->default("");
            $table->string('instagram',40)->default("");
            $table->string('aboutme',500)->default("");
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('channels');
        Schema::dropIfExists('users');
    }
}
