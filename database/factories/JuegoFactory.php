<?php

use Faker\Generator as Faker;

$factory->define(App\Juego::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'minimum_age' => $faker->randomDigit,
        'thumbnail' => $faker->image('public/storage/images/',285,380, null, false), // secret
        'banner_image' => $faker->image('public/storage/images/',1300,380, null, false), // secret
    ];
});
