<?php

use Faker\Generator as Faker;

$factory->define(App\chat::class, function (Faker $faker) {
    return [
        'message' => $faker->sentence,
        'user_id' => rand(1,50)
    ];
});
