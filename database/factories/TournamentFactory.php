<?php

use Faker\Generator as Faker;

$factory->define(App\Tournament::class, function (Faker $faker) {
    return [
        'name'=>$faker->userName,
        'max_players'=>('16'),
        'actual_players'=>rand(0,16),
        'platform'=>random_int(1,3),
        'juego_id'=>random_int(1,10),
        'open_date'=>$faker->dateTime,
        'close_date'=>$faker->dateTime,
        'play_date'=>$faker->dateTime,
    ];
});
