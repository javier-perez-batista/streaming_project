<?php

use Faker\Generator as Faker;

$factory->define(App\GameAccount::class, function (Faker $faker) {
    return [
        'id_juego' => rand(1,50),
        'id_user' => rand(1,50),
        'name_account' => $faker->userName,
    ];
});
