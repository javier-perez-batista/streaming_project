<?php

use Faker\Generator as Faker;

$factory->define(App\Video::class, function (Faker $faker) {
    $array = ['VODAFONE GIANTS VS DRAGONS E.C. _ Superliga Orange _ Partido 2 _ Split Verano [2018]','Plup vs Hungrybox - GOML 2018 - Melee Grand Finals','CS_GO - Team Liquid vs. Astralis [Dust2] Map 1 - Grand Final - ESL Pro League S7 Day 6','Ninja First Wins at Las Vegas Fortnite Tournament - Fortnite Battle Royale Highlights'];
    return [
        'name' => $array[rand(0,sizeof($array)-1)],
        'juego_id' => rand(1,50),
        'channel_id' => rand(1,50),
        'thumbnail' => $faker->image('public/storage/images/',400,300, null, false),
    ];
});

