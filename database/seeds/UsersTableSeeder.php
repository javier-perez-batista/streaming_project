<?php

use Illuminate\Database\Seeder;
use App\Channel;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 50)->create();
        /*
         * ->each(function ($u) {
            $u->channels()->save(factory(App\Channel::class)->make());
        }
         * */
    }
}
