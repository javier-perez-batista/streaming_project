<?php

use Illuminate\Database\Seeder;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Channel::class, 50)->create();
        /*
         * ->each(function ($u) {
            $u->users()->save(factory(App\User::class)->make());
        })
         * */
    }
}
