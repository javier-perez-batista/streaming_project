<?php

use App\Platform;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlatformsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('platforms')->insert([
            'name' => 'pc',
        ]);
        DB::table('platforms')->insert([
            'name' => 'ps4',
        ]);
        DB::table('platforms')->insert([
            'name' => 'xbox',
        ]);

    }
}
