<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Response;


//------------------User---------------------------
Route::get('/user/{id}','UserController@showProfile');
Route::get('/user/addfriend/{id}','UserController@addFriend');
Route::get('/user/unfriend/{id}','UserController@unFriend');
Route::get('/user/reload/friends','UserController@reloadFriends');
Route::get('/user/actualiza/aboutme/{contenido}','UserController@actualizaAboutMe');
Route::post('/user/{id}','UserController@followProfile');


//------------------Video---------------------------
Route::get('/video/canal/{filename}','videoStreamController@loadVideo');
Route::get('/video/play/{id}','videoStreamController@playVideo');
Route::get('/video/prueba','videoStreamController@prueba');

//------------------Channel---------------------------
Route::get('/','ChannelController@index')->name('index');
Route::post('/','ChannelController@search');
Route::get('/channel/{id}','ChannelController@showChannel');
Route::get('/channel/follow/{id_channel}','ChannelController@followChannel');
Route::get('/channel/unfollow/{id_channel}','ChannelController@unfollowChannel');
Route::get('/channel/reload/channels/','ChannelController@reloadChannels');
Route::post('/channel/{id}','ChannelController@follow');

//------------------Juegos---------------------------
Route::get('/juegos','JuegoController@index');
Route::get('/juegos/{id}','JuegoController@show');
Route::get('/juegos/addFavoritos/{id}','JuegoController@addFavoritos');
Route::get('/juegos/quitarFavoritos/{id}','JuegoController@quitarFavoritos');
Route::get('/juegos/cambiarPestanya/{juego}/{id}','JuegoController@cambiarPestanya');

//------------------Torneos---------------------------
Route::get('/torneos','TournamentController@index');
Route::get('/torneos/{platform}','AjaxController@cambiaPestanya');
Route::get('/torneos/jugar/{id_torneo}','AjaxController@jugarTorneo');
Route::get('/torneos/dejar/{id_torneo}','AjaxController@dejarTorneo');
Route::get('/torneos/juegos/input/{input}','AjaxController@filtrarJuegos');
Route::get('/torneos/juego/{id_juego}/plataforma/{id_plataforma}/','AjaxController@juegosFiltrados');
Route::post('/torneos','TournamentController@unirse');
//--------------------Chat----------------------------------
Route::get('/traeChat','ChatController@nuevoTexto');
Route::get('/traeChat/{texto}','ChatController@enviarTexto');
//------------------Jobs----------------------------------
Route::get('/seguir/{id}','ChannelController@follow');

//------------------Home---------------------------
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/explorar','HomeController@explorar');
Route::get('/index/ver/juegos/','AjaxController@verJuegos');
Route::get('/index/ver/channels/','AjaxController@verChannels');
Route::get('/index/ver/users/','AjaxController@verUsers');
Route::get('/logout','AjaxController@logOut');

Auth::routes();